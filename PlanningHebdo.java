//salut
package application_java_app;

import java.util.ArrayList;

public class PlanningHebdo implements Serializable{
	private ArrayList<DayPlanning> day;
private final long serialVersionUID =6L;
	public PlanningHebdo() {
		day = new ArrayList<>(5);
	}

	public PlanningHebdo(ArrayList<DayPlanning> planParam) {
		for(int i = 0; i < day.size(); i ++){
			day.add(i, planParam.get(i));
		}		
	}

	public ArrayList<DayPlanning> getPlanning() {
		return day;
	}

	public void setPlanning(ArrayList<DayPlanning> newList) {
		for(int i = 0; i < day.size(); i ++){
			day.add(i, newList.get(i));
		}
	}

	public void addPlanningDay(DayPlanning planDayParam, int index) {
		day.add(index, planDayParam);
	}

	public void displayPlanHebdo() {
		for(DayPlanning d : day){
			d.displayDayPlan();
		}
	}

}
