import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Stub {
	
	public static void Stub() {
		
		DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("dd / MM / yyyy");
		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("HH : mm");
		
		LocalDate firstDate = LocalDate.parse("17 / 05 / 2021", formatter1);
		LocalDate secondDate = LocalDate.parse("18 / 05 / 2021",formatter1);
		LocalDate thirdDate = LocalDate.parse("19 / 05 / 2021",formatter1);
		LocalDate fourthDate = LocalDate.parse("20 / 05 / 2021",formatter1);
		LocalDate fiveDate = LocalDate.parse("21 / 05 / 2021",formatter1);
		LocalDate sixDate = LocalDate.parse("22 / 05 / 2021",formatter1);
		LocalDate sevenDate = LocalDate.parse("23 / 05 / 2021",formatter1);
		LocalDate eightDate = LocalDate.parse("24 / 05 / 2021",formatter1);
		LocalDate nineDate = LocalDate.parse("25 / 05 / 2021",formatter1);
		LocalDate tenDate = LocalDate.parse("26 / 05 / 2021",formatter1);
		LocalDate elevenDate = LocalDate.parse("27 / 05 / 2021",formatter1);
		LocalDate twelveDate = LocalDate.parse("28 / 05 / 2021",formatter1);
		LocalDate thirteenDate = LocalDate.parse("29 / 05 / 2021",formatter1);
		LocalDate fourteenDate = LocalDate.parse("30 / 05 / 2021",formatter1);
		
		LocalTime firstTimeArr = LocalTime.parse("06 : 00",formatter2);
		LocalTime secondTimeArr = LocalTime.parse("06 : 15",formatter2);
		LocalTime thirdTimeArr = LocalTime.parse("06 : 30",formatter2);
		LocalTime fourthTimeArr = LocalTime.parse("06 : 45",formatter2);
		LocalTime fiveTimeArr = LocalTime.parse("07 : 00",formatter2);
		LocalTime sixTimeArr = LocalTime.parse("07 : 15",formatter2);
		LocalTime sevenTimeArr = LocalTime.parse("07 : 30",formatter2);
		LocalTime eightTimeArr = LocalTime.parse("07 : 45",formatter2);
		LocalTime nineTimeArr = LocalTime.parse("08 : 00",formatter2);
		LocalTime tenTimeArr = LocalTime.parse("08 : 15",formatter2);
		LocalTime elevenTimeArr = LocalTime.parse("08 : 30",formatter2);
		LocalTime twelveTimeArr = LocalTime.parse("08 : 45",formatter2);
		LocalTime thirteenTimeArr = LocalTime.parse("09 : 00",formatter2);
		LocalTime fourthteenTimeArr = LocalTime.parse("10 : 00",formatter2);
		
		LocalTime firstTimeDep = LocalTime.parse("16 : 00",formatter2);
		LocalTime secondTimeDep = LocalTime.parse("16 : 15",formatter2);
		LocalTime thirdTimeDep = LocalTime.parse("16 : 30",formatter2);
		LocalTime fourthTimeDep = LocalTime.parse("16 : 45",formatter2);
		LocalTime fiveTimeDep = LocalTime.parse("17 : 00",formatter2);
		LocalTime sixTimeDep = LocalTime.parse("17 : 15",formatter2);
		LocalTime sevenTimeDep = LocalTime.parse("17 : 30",formatter2);
		LocalTime eightTimeDep = LocalTime.parse("17 : 45",formatter2);
		LocalTime nineTimeDep = LocalTime.parse("18 : 00",formatter2);
		LocalTime tenTimeDep = LocalTime.parse("18 : 15",formatter2);
		LocalTime elevenTimeDep = LocalTime.parse("18 : 30",formatter2);
		LocalTime twelveTimeDep = LocalTime.parse("18 : 45",formatter2);
		LocalTime thirteenTimeDep = LocalTime.parse("19 : 00",formatter2);
		LocalTime fourthteenTimeDep = LocalTime.parse("20 : 00",formatter2);
		
		Company secondCompany = new Company("Facebuck");
		
		Departement firstDep = new Departement("Recherche biologique");
		Departement secondDep = new Departement("Recherche informatique");
		Departement thirdDep = new Departement("Recherche développement durable");
		Departement fourthDep = new Departement("Ressource humaines");
		Departement fiveDep = new Departement("Droit international");
		Departement sixDep = new Departement("Droit des affaires");
		
		Employee firstEmp = new Employee("Durant", "Johnattan");
		Employee secondEmp = new Employee("Smith", "Marc");
		Employee thirdEmp = new Employee("Tatam", "John");
		Employee fourthEmp = new Employee("Barret", "Simon");
		Employee fiveEmp = new Employee("Soumier", "Yasmina");
		Employee sixEmp = new Employee("Laroche", "Jean");
		Employee sevenEmp = new Employee("Zedong", "Mao");
		Employee eightEmp = new Employee("Ngatimo", "Axel");
		Employee nineEmp = new Employee("Ledanois", "Benjamin");
		Employee tenEmp = new Employee("Roura","Antoine");
		Employee elevenEmp = new Employee("Courné","Jules");
		Employee twelveEmp = new Employee("Jolie","Emilie");
		
		PlanningHebdo firstPlanH = new PlanningHebdo();
		PlanningHebdo secondPlanH = new PlanningHebdo();
		PlanningHebdo thirdPlanH = new PlanningHebdo();
		PlanningHebdo fourthPlanH = new PlanningHebdo();
		PlanningHebdo fivePlanH = new PlanningHebdo();
		PlanningHebdo sixPlanH = new PlanningHebdo();
		PlanningHebdo sevenPlanH = new PlanningHebdo();
		PlanningHebdo eightPlanH = new PlanningHebdo();
		
		DayPlanning firstDay1 = new DayPlanning(firstDate, firstTimeArr, firstTimeDep);
		DayPlanning secondDay1 = new DayPlanning(secondDate, tenTimeArr, twelveTimeDep);
		DayPlanning thirdDay1 = new DayPlanning(thirdDate, secondTimeArr, fiveTimeDep);
		DayPlanning fourthDay1 = new DayPlanning(fourthDate, sevenTimeArr, sevenTimeDep);
		DayPlanning fiveDay1 = new DayPlanning(fiveDate, fiveTimeArr, firstTimeDep);
		DayPlanning sixDay1 = new DayPlanning(sixDate, eightTimeArr, fourthTimeDep);
		DayPlanning sevenDay1 = new DayPlanning(sevenDate, twelveTimeArr, sixTimeDep);
		DayPlanning eightDay1 = new DayPlanning(eightDate, tenTimeArr, eightTimeDep);
		DayPlanning nineDay1 = new DayPlanning(nineDate, fourthteenTimeArr, tenTimeDep);
		DayPlanning tenDay1 = new DayPlanning(tenDate, fourthTimeArr, fourthteenTimeDep);
		DayPlanning elevenDay1 = new DayPlanning(elevenDate, thirdTimeArr, thirteenTimeDep);
		DayPlanning twelveDay1 = new DayPlanning(twelveDate, sixTimeArr, secondTimeDep);
		DayPlanning thirteenDay1 = new DayPlanning(thirteenDate, secondTimeArr, twelveTimeDep);
		DayPlanning fourtheenDay1 = new DayPlanning(fourteenDate, sevenTimeArr, thirdTimeDep);
		
		DayPlanning firstDay2 = new DayPlanning(firstDate, thirteenTimeArr, twelveTimeDep);
		DayPlanning secondDay2 = new DayPlanning(secondDate, sevenTimeArr, firstTimeDep);
		DayPlanning thirdDay2 = new DayPlanning(thirdDate, twelveTimeArr, fiveTimeDep);
		DayPlanning fourthDay2 = new DayPlanning(fourthDate,sixTimeArr , sixTimeDep);
		DayPlanning fiveDay2 = new DayPlanning(fiveDate, firstTimeArr, firstTimeDep);
		DayPlanning sixDay2 = new DayPlanning(sixDate, thirdTimeArr, fourthteenTimeDep);
		DayPlanning sevenDay2 = new DayPlanning(sevenDate,fourthTimeArr , sevenTimeDep);
		DayPlanning eightDay2 = new DayPlanning(eightDate, secondTimeArr, eightTimeDep);
		DayPlanning nineDay2 = new DayPlanning(nineDate, eightTimeArr, tenTimeDep);
		DayPlanning tenDay2 = new DayPlanning(tenDate, elevenTimeArr, fourthTimeDep);
		DayPlanning elevenDay2 = new DayPlanning(elevenDate, nineTimeArr, thirteenTimeDep);
		DayPlanning twelveDay2 = new DayPlanning(twelveDate, fourthteenTimeArr, thirdTimeDep);
		DayPlanning thirteenDay2 = new DayPlanning(thirteenDate, tenTimeArr, twelveTimeDep);
		DayPlanning fourtheenDay2 = new DayPlanning(fourteenDate,fiveTimeArr , secondTimeDep);
		
		DayPlanning firstDay3 = new DayPlanning(firstDate, tenTimeArr, eightTimeDep);
		DayPlanning secondDay3 = new DayPlanning(secondDate, fourthteenTimeArr, sevenTimeDep);
		DayPlanning thirdDay3 = new DayPlanning(thirdDate, elevenTimeArr, thirteenTimeDep);
		DayPlanning fourthDay3 = new DayPlanning(fourthDate, nineTimeArr, twelveTimeDep);
		DayPlanning fiveDay3 = new DayPlanning(fiveDate, eightTimeArr,  firstTimeDep);
		DayPlanning sixDay3 = new DayPlanning(sixDate, secondTimeArr, secondTimeDep);
		DayPlanning sevenDay3 = new DayPlanning(sevenDate, fourthTimeArr, sixTimeDep);
		DayPlanning eightDay3 = new DayPlanning(eightDate, fiveTimeArr, firstTimeDep);
		DayPlanning nineDay3 = new DayPlanning(nineDate, thirdTimeArr, tenTimeDep);
		DayPlanning tenDay3 = new DayPlanning(tenDate, twelveTimeArr, fiveTimeDep);
		DayPlanning elevenDay3 = new DayPlanning(elevenDate, sixTimeArr, fiveTimeDep);
		DayPlanning twelveDay3 = new DayPlanning(twelveDate, thirdTimeArr, fourthTimeDep);
		DayPlanning thirteenDay3 = new DayPlanning(thirteenDate, secondTimeArr, thirdTimeDep);
		DayPlanning fourtheenDay3 = new DayPlanning(fourteenDate, sevenTimeArr, fiveTimeDep);
		
		
		firstPlanH.addPlanningDay(firstDay2, 0);
		firstPlanH.addPlanningDay(secondDay1, 1);
		firstPlanH.addPlanningDay(thirdDay3, 2);
		firstPlanH.addPlanningDay(fourthDay2, 3);
		firstPlanH.addPlanningDay(fiveDay2, 4);
		firstPlanH.addPlanningDay(sixDay3, 5);
		firstPlanH.addPlanningDay(sevenDay1, 6);
		
		thirdPlanH.addPlanningDay(firstDay3, 0);
		thirdPlanH.addPlanningDay(secondDay2, 1);
		thirdPlanH.addPlanningDay(thirdDay1, 2);
		thirdPlanH.addPlanningDay(fourthDay3, 3);
		thirdPlanH.addPlanningDay(fiveDay2, 4);
		thirdPlanH.addPlanningDay(sixDay1, 5);
		thirdPlanH.addPlanningDay(sevenDay3, 6);
		
		sevenPlanH.addPlanningDay(firstDay2, 0);
		sevenPlanH.addPlanningDay(secondDay3, 1);
		sevenPlanH.addPlanningDay(thirdDay1, 2);
		sevenPlanH.addPlanningDay(fourthDay3, 3);
		sevenPlanH.addPlanningDay(fiveDay2, 4);
		sevenPlanH.addPlanningDay(sixDay2, 5);
		sevenPlanH.addPlanningDay(sevenDay2, 6);
		
		fivePlanH.addPlanningDay(firstDay1, 0);
		fivePlanH.addPlanningDay(secondDay2, 1);
		fivePlanH.addPlanningDay(thirdDay3, 2);
		fivePlanH.addPlanningDay(fourthDay1, 3);
		fivePlanH.addPlanningDay(fiveDay2, 4);
		fivePlanH.addPlanningDay(sixDay3, 5);
		fivePlanH.addPlanningDay(sevenDay2, 6);
		
		secondPlanH.addPlanningDay(eightDay1, 0);
		secondPlanH.addPlanningDay(nineDay3, 1);
		secondPlanH.addPlanningDay(tenDay2, 2);
		secondPlanH.addPlanningDay(elevenDay1, 3);
		secondPlanH.addPlanningDay(twelveDay3, 4);
		secondPlanH.addPlanningDay(thirteenDay2, 5);
		secondPlanH.addPlanningDay(fourtheenDay1, 6);
		
		fourthPlanH.addPlanningDay(eightDay3, 0);
		fourthPlanH.addPlanningDay(nineDay2, 1);
		fourthPlanH.addPlanningDay(tenDay1, 2);
		fourthPlanH.addPlanningDay(elevenDay3, 3);
		fourthPlanH.addPlanningDay(twelveDay2, 4);
		fourthPlanH.addPlanningDay(thirteenDay1, 5);
		fourthPlanH.addPlanningDay(fourtheenDay3, 6);
		
		sixPlanH.addPlanningDay(eightDay1, 0);
		sixPlanH.addPlanningDay(nineDay2, 1);
		sixPlanH.addPlanningDay(tenDay3, 2);
		sixPlanH.addPlanningDay(elevenDay1, 3);
		sixPlanH.addPlanningDay(twelveDay2, 4);
		sixPlanH.addPlanningDay(thirteenDay3, 5);
		sixPlanH.addPlanningDay(fourtheenDay2, 6);
		
		eightPlanH.addPlanningDay(eightDay3, 0);
		eightPlanH.addPlanningDay(nineDay3, 1);
		eightPlanH.addPlanningDay(tenDay3, 2);
		eightPlanH.addPlanningDay(elevenDay2, 3);
		eightPlanH.addPlanningDay(twelveDay2, 4);
		eightPlanH.addPlanningDay(thirteenDay1, 5);
		eightPlanH.addPlanningDay(fourtheenDay2, 6);
		
		firstEmp.addPlanning(firstPlanH);
		firstEmp.addPlanning(secondPlanH);		
		secondEmp.addPlanning(firstPlanH);
		secondEmp.addPlanning(fourthPlanH);		
		thirdEmp.addPlanning(firstPlanH);
		thirdEmp.addPlanning(sixPlanH);		
		fourthEmp.addPlanning(thirdPlanH);
		fourthEmp.addPlanning(eightPlanH);	
		fiveEmp.addPlanning(thirdPlanH);
		fiveEmp.addPlanning(secondPlanH);	
		sixEmp.addPlanning(thirdPlanH);
		sixEmp.addPlanning(fourthPlanH);		
		sevenEmp.addPlanning(fivePlanH);
		sevenEmp.addPlanning(secondPlanH);	
		eightEmp.addPlanning(fivePlanH);
		eightEmp.addPlanning(sixPlanH);	
		nineEmp.addPlanning(fivePlanH);
		nineEmp.addPlanning(fourthPlanH);
		tenEmp.addPlanning(sevenPlanH);
		tenEmp.addPlanning(fourthPlanH);
		elevenEmp.addPlanning(sevenPlanH);
		elevenEmp.addPlanning(sixPlanH);
		twelveEmp.addPlanning(sevenPlanH);
		twelveEmp.addPlanning(secondPlanH);
		
		firstDep.addEmployee(firstEmp);
		firstDep.addEmployee(secondEmp);
		secondDep.addEmployee(thirdEmp);
		secondDep.addEmployee(fourthEmp);
		thirdDep.addEmployee(fiveEmp);
		thirdDep.addEmployee(sixEmp);
		fourthDep.addEmployee(sevenEmp);
		fourthDep.addEmployee(eightEmp);
		fiveDep.addEmployee(nineEmp);
		fiveDep.addEmployee(tenEmp);
		sixDep.addEmployee(twelveEmp);
		sixDep.addEmployee(elevenEmp);
		

		secondCompany.addDepartement(thirdDep);
		secondCompany.addDepartement(fourthDep);
		secondCompany.addDepartement(firstDep);
		secondCompany.addDepartement(secondDep);
		secondCompany.addDepartement(fiveDep);
		secondCompany.addDepartement(sixDep);
		
		
		Serialisation.SerializeCompany(secondCompany);
		Serialisation.SerializeDepartement(firstDep);
		Serialisation.SerializeDepartement(secondDep);
		Serialisation.SerializeDepartement(thirdDep);
		Serialisation.SerializeDepartement(fourthDep);
		Serialisation.SerializeDepartement(fiveDep);
		Serialisation.SerializeDepartement(sixDep);
		Serialisation.SerializeEmployee(firstEmp);
		Serialisation.SerializeEmployee(secondEmp);
		Serialisation.SerializeEmployee(thirdEmp);
		Serialisation.SerializeEmployee(fourthEmp);
		Serialisation.SerializeEmployee(fiveEmp);
		Serialisation.SerializeEmployee(sixEmp);
		Serialisation.SerializeEmployee(sevenEmp);
		Serialisation.SerializeEmployee(eightEmp);
		Serialisation.SerializeEmployee(nineEmp);
		Serialisation.SerializeEmployee(tenEmp);
		Serialisation.SerializeEmployee(twelveEmp);
		Serialisation.SerializePlanningH(firstPlanH);
		Serialisation.SerializePlanningH(secondPlanH);
		Serialisation.SerializePlanningH(thirdPlanH);
		Serialisation.SerializePlanningH(fourthPlanH);
		Serialisation.SerializePlanningH(fivePlanH);
		Serialisation.SerializePlanningH(sixPlanH);
		Serialisation.SerializePlanningH(sevenPlanH);
		Serialisation.SerializePlanningH(eightPlanH);
		Serialisation.SerializeDayPlanning(firstDay1);
		Serialisation.SerializeDayPlanning(secondDay1);
		Serialisation.SerializeDayPlanning(thirdDay1);
		Serialisation.SerializeDayPlanning(fourthDay1);
		Serialisation.SerializeDayPlanning(fiveDay1);
		Serialisation.SerializeDayPlanning(sixDay1);
		Serialisation.SerializeDayPlanning(sevenDay1);
		Serialisation.SerializeDayPlanning(eightDay1);
		Serialisation.SerializeDayPlanning(nineDay1);
		Serialisation.SerializeDayPlanning(tenDay1);
		Serialisation.SerializeDayPlanning(elevenDay1);
		Serialisation.SerializeDayPlanning(twelveDay1);
		Serialisation.SerializeDayPlanning(thirteenDay1);
		Serialisation.SerializeDayPlanning(fourtheenDay1);
		Serialisation.SerializeDayPlanning(firstDay2);
		Serialisation.SerializeDayPlanning(secondDay2);
		Serialisation.SerializeDayPlanning(thirdDay2);
		Serialisation.SerializeDayPlanning(fourthDay2);
		Serialisation.SerializeDayPlanning(fiveDay2);
		Serialisation.SerializeDayPlanning(sixDay2);
		Serialisation.SerializeDayPlanning(sevenDay2);
		Serialisation.SerializeDayPlanning(eightDay2);
		Serialisation.SerializeDayPlanning(nineDay2);
		Serialisation.SerializeDayPlanning(tenDay2);
		Serialisation.SerializeDayPlanning(elevenDay2);
		Serialisation.SerializeDayPlanning(twelveDay2);
		Serialisation.SerializeDayPlanning(thirteenDay2);
		Serialisation.SerializeDayPlanning(fourtheenDay2);
		Serialisation.SerializeDayPlanning(firstDay3);
		Serialisation.SerializeDayPlanning(secondDay3);
		Serialisation.SerializeDayPlanning(thirdDay3);
		Serialisation.SerializeDayPlanning(fourthDay3);
		Serialisation.SerializeDayPlanning(fiveDay3);
		Serialisation.SerializeDayPlanning(sixDay3);
		Serialisation.SerializeDayPlanning(sevenDay3);
		Serialisation.SerializeDayPlanning(eightDay3);
		Serialisation.SerializeDayPlanning(nineDay3);
		Serialisation.SerializeDayPlanning(tenDay3);
		Serialisation.SerializeDayPlanning(elevenDay3);
		Serialisation.SerializeDayPlanning(twelveDay3);
		Serialisation.SerializeDayPlanning(thirteenDay3);
		Serialisation.SerializeDayPlanning(fourtheenDay3);
	}
}