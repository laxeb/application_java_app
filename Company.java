package application_java_app;

import java.util.ArrayList;

public class Company implements Serializable{
	private String NameComp;
	private final long serialVersionUID = 1L;
	private ArrayList<Departement> departement ;

	/**
	 * @brief default builder
	 */
	public Company() {
		departement = new ArrayList<Departement>();
	}
	
	/**
	 * @brief builder with the name of the company
	 * @param NameComp the name
	 */
	public Company(String NameComp) {
		this.NameComp = NameComp;
		departement = new ArrayList<Departement>();
	}
	/**
	 * @brief builder with the name of the company and the list of departments
	 * @param NameComp the name of the company
	 * @param departement the list of departments
	 */
	public Company(String NameComp, ArrayList<Departement> departement) {
		this.NameComp = NameComp;
		this.departement = new ArrayList<Departement>(departement);
	}
	
	/**
	 * @brief NameComp getter
	 */
	public String getNameComp() {
		return NameComp;
	}

	/**
	 * @brief List of departments getter
	 */
	public ArrayList<Departement> getListDep() {
		return departement;
	}
	
	/**
	 * @brief adds a department to the list of departments
	 * @param DepParam the department to add
	 */
	public void addDepartement(Departement DepParam) {
		if(departement.contains(DepParam)){
			throw new IllegalArgumentException("Departement d�j� r�ferenc� !");
		}
		else {
			departement.add(DepParam);
		}
	}
	
	/**
	 * @brief NameComp setter
	 */
	public void setNameComp(String NameComp) {
		this.NameComp = NameComp;
	}

	public void setListDepartement(ArrayList<Departement> departement) {
		this.departement = new ArrayList<Departement>(departement);
	}
	
	
	/**
	 * @brief displays the list of employees
	 */
	public void displayEmployees() {
		for (Departement dep : departement) {
			dep.displayEmployees();
		}
	}
	
	/**
	 * @brief change a company object to string
	 */
	public String toString() {
		String str = "Name of the Company : " + getNameComp() + "\nListe des d�partements : " + departement;
		return str;
	}

}
