import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

public class DayPlanning implements Serializable{
	private final long serialVersionUID = 3L;
	private LocalDate dateDay;

	private LocalTime arrTime;

	private LocalTime depTime;

	public DayPlanning() {

	}
	
	public DayPlanning(LocalDate dateDay, LocalTime arrTime, LocalTime depTime){
		this.dateDay = dateDay;
		this.arrTime = arrTime;
		this.depTime = depTime;
	}
	
	
	public DayPlanning(DayPlanning dayPlan) {
		dateDay = dayPlan.dateDay;
		arrTime = dayPlan.arrTime;
		depTime = dayPlan.depTime;
	}

	public LocalTime getArrTime() {
		return arrTime;
	}

	public LocalTime getDepTime() {
		return depTime;
	}

	public LocalDate getDate() {
		return dateDay;
	}

	public void setArrTime(LocalTime newArrTime) {
		arrTime = newArrTime;
	}

	public void setDepTime(LocalTime newDepTime) {
		depTime = newDepTime;
	}

	public void setDate(LocalDate newDate) {
		dateDay = newDate;
	}

	public void displayDayPlan() {
		System.out.println("Date : " + dateDay);
		System.out.println("Heure d'arriv�e : " + arrTime);
		System.out.println("Heure de de d�pard : " + depTime);
	}
	
	
	public static void main(String args[]){
		DayPlanning day = new DayPlanning();

		day.setArrTime(LocalTime.now());
		day.setDepTime(LocalTime.now());
		day.setDate(LocalDate.now());

		day.displayDayPlan();
	}
}
