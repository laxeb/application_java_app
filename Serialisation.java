import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Serialisation{
	
	public static ArrayList<Company> ListComp = new ArrayList<Company>();
	public static ArrayList<Departement> ListDep = new ArrayList<Departement>();
	public static ArrayList<Employee> ListEmp = new ArrayList<Employee>();
	public static ArrayList<PlanningHebdo> ListPlanH = new ArrayList<PlanningHebdo>();
	public static ArrayList<DayPlanning> ListDayPlan = new ArrayList<DayPlanning>();
	public static ArrayList<Scoring> ListScoring = new ArrayList<Scoring>();
	
	public static File fichierCompany = new File("src/Company.ser");
	public static File fichierDepartement = new File("src/Departement.ser");
	public static File fichierEmployee = new File("src/Employee.ser");
	public static File fichierPlanningHebdo = new File("src/PlanningHebdo.ser");
	public static File fichierDayPlanning = new File("src/DayPlanning.ser");
	public static File fichierScoring = new File("src/Scoring.ser");
	
	
	
	public static void SerializeCompany(Company comp) {
		ListComp.add(comp);
		try {
			ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierCompany)) ;
			oos.writeObject(ListComp);
			oos.close();
		}
		catch(IOException exc) {
			exc.printStackTrace();
		}
	}
	
	public static void SerializeDepartement(Departement dep) {
		ListDep.add(dep);
		try {
			ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierDepartement)) ;
			oos.writeObject(ListDep);
			oos.close();
		}
		catch(IOException exc) {
			exc.printStackTrace();
		}
	}
	
	public static void SerializeEmployee(Employee emp) {
		ListEmp.add(emp);
		try {
			ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierEmployee)) ;
			oos.writeObject(ListEmp);
			oos.close();
		}
		catch(IOException exc) {
			exc.printStackTrace();
		}
	}
	
	public static void SerializePlanningH(PlanningHebdo PlanH) {
		ListPlanH.add(PlanH);
		try {
			ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierPlanningHebdo)) ;
			oos.writeObject(ListPlanH);
			oos.close();
		}
		catch(IOException exc) {
			exc.printStackTrace();
		}
	}
	
	public static void SerializeDayPlanning(DayPlanning dayP) {
		ListDayPlan.add(dayP);
		try {
			ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierDayPlanning)) ;
			oos.writeObject(ListDayPlan);
			oos.close();
		}
		catch(IOException exc) {
			exc.printStackTrace();
		}
	}
	
	public static void SerializeScoring(Scoring scor) {
		ListScoring.add(scor);
		try {
			ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierScoring)) ;
			oos.writeObject(ListScoring);
			oos.close();
		}
		catch(IOException exc) {
			exc.printStackTrace();
		}
	}
	
	
	public static ArrayList<Company> DeserializeCompany() {
		try {
			
			ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(fichierCompany)) ;
			ListComp =  (ArrayList<Company>) ois.readObject();
			ois.close();
		}
		catch(IOException exc) {
			exc.printStackTrace();
		}
		catch(ClassNotFoundException ex)
		{
			ex.printStackTrace();
		}
		return ListComp;
	}
	
	
	public static ArrayList<Departement> DeserializeDepartement() {
		try {
			ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(fichierDepartement)) ;
			ListDep = (ArrayList<Departement>) ois.readObject();
			ois.close();
		}
		catch(IOException exc) {
			exc.printStackTrace();
		}
		catch(ClassNotFoundException ex)
		{
			ex.printStackTrace();
		}
		return ListDep;
	}
	
	public static ArrayList<Employee> DeserializeEmployee() {
		try {
			ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(fichierEmployee)) ;
			ListEmp = (ArrayList<Employee>) ois.readObject();
			ois.close();
		}
		catch(IOException exc) {
			exc.printStackTrace();
		}
		catch(ClassNotFoundException ex)
		{
			ex.printStackTrace();
		}
		return ListEmp;
	}
	
	public static ArrayList<PlanningHebdo> DeserializePlanningHebdo() {
		try {
			ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(fichierPlanningHebdo)) ;
			ListPlanH = (ArrayList<PlanningHebdo>) ois.readObject();
			ois.close();
		}
		catch(IOException exc) {
			exc.printStackTrace();
		}
		catch(ClassNotFoundException ex)
		{
			ex.printStackTrace();
		}
		return ListPlanH;
	}
	
	
	public static ArrayList<DayPlanning> DeserializeDayPlanning() {
		try {
			ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(fichierDayPlanning)) ;
			ListDayPlan = (ArrayList<DayPlanning>) ois.readObject();
			ois.close();
		}
		catch(IOException exc) {
			exc.printStackTrace();
		}
		catch(ClassNotFoundException ex)
		{
			ex.printStackTrace();
		}
		return ListDayPlan;
	}
	
	
	public static ArrayList<Scoring> DeserializeScoring() {
		try {
			ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(fichierScoring)) ;
			ListScoring = (ArrayList<Scoring>) ois.readObject();
			ois.close();
		}
		catch(IOException exc) {
			exc.printStackTrace();
		}
		catch(ClassNotFoundException ex)
		{
			ex.printStackTrace();
		}
		return ListScoring;
	}
	
	
}