package application_java_app;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Scoring implements Serializable{
	private LocalDate dataScore;
	private final long serialVersionUID = 5L;
	private LocalTime  arrTimeScore;

	private LocalTime depTimeScore;

	private Employee  employee ;

	public LocalDate getDate() {
		return dataScore;
	}

	public LocalTime getArrTime() {
		return arrTimeScore;
	}

	public LocalTime getDepTim() {
		return depTimeScore;
	}

	public LocalTime getNBHour() {
		return null;
	}

	public void setArrTime(LocalTime newArr) {
           arrTimeScore = newArr;
	}

	public void setDepTime(LocalTime newDep) {
             depTimeScore =newDep ;
	}

	public void setDateScore(LocalDate newDate) {
             dataScore= newDate;
	}
 public void setEmployee(Employee emp) {
	 employee =emp ;
 }
public Employee getEmployee() {
	return employee ;
}
}
