import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JOptionPane;


public class ActionPointeuse implements ActionListener{
	
	//private Serialisation ser = new Serialisation(); //Initialisation d'un objet Serialisation
	
	private IHMPointeuse pointeuse;
	
	/*Constructeur servant à savoir de quelle objet vient l'action*/
	public ActionPointeuse(IHMPointeuse pointeuse) {
		super();
		this.pointeuse = pointeuse;
	}
	
	
	/*méthode qui extrait les nom et prenom des employees des fichier et renvoie la concatenation des deux*/
	public String[] getNameEmp() {
		int i = 1;
		String[] string = new String[Serialisation.DeserializeEmployee().size()+1];
		string[0] = null;
		for(Employee empTemp : Serialisation.DeserializeEmployee()) {
			string[i] = empTemp.getFirstName() + " " + empTemp.getName();
			i++;
		}
		return string;
	}
	
	
	public void actionPerformed(ActionEvent e) {
		
		Serialisation.DeserializeEmployee();//On déserialise le fichier employee
		
		/*Si on appuie sur checkIn*/
		if(e.getActionCommand() == "checkIn") {
			
			/*Le nouvel objet Scoring prend la date et l'heure actuelle*/
			Scoring scoring = new Scoring();
			scoring.setDateScore(pointeuse.getDate());
			scoring.setArrTime(pointeuse.getAroundTime());	
			
			/*Si le champ contenant l'ID n'est pas vide et le champ contenant les nom et prenom est vide*/
			if(pointeuse.gettxtID().getText().length() != 0 && pointeuse.getComboSelected() == null) {
				try {
					
					int ID, cmpt = 0;
					ID = Integer.parseInt(pointeuse.gettxtID().getText()); //ID contient la valeur rentré dans le champ de l'ID
					
					/*empTemp prend la valeur de chaque objet employee contenue dans le fichier employee*/
					for(Employee empTemp : Serialisation.DeserializeEmployee()) {
						
						/*Si l'ID d'un employé du fichier correspond à l'ID rentré dans la pointeuse*/
						if(empTemp.getIdEmp() == ID) {
							JOptionPane.showMessageDialog(pointeuse, "Validated Score", "Validation",javax.swing.JOptionPane.ERROR_MESSAGE);
							scoring.setEmployee(empTemp);//On ajoute l'employé correspondant dans le nouvel objet scoring.
						}
						
						/*Pour chaque employee du fichier n'ayant pas l'ID rentré dans la pointeuse*/
						if(empTemp.getIdEmp() != ID) {
							cmpt++; //on incrémente cmpt
						}
					}
					
					/*Si aucun ID rentré dans la pointeuse ne correspond au employee du fichier*/
					if(cmpt == Serialisation.DeserializeEmployee().size()) {
						JOptionPane.showMessageDialog(pointeuse, " Acknowledged ID", "Erreur",javax.swing.JOptionPane.ERROR_MESSAGE);
					}
				}
				
				/*Exception sur le format du nombre rentré dans le champ ID de la pointeuse*/
				catch(NumberFormatException nbr) {
					JOptionPane.showMessageDialog(pointeuse, "Format Error ID " , "Erreur",javax.swing.JOptionPane.ERROR_MESSAGE);
					nbr.printStackTrace();
				}
			}
			
			/*Si le champ contenant l'ID est vide et l'utilisateur a sélectionné un nom et prenom*/
			if(pointeuse.gettxtID().getText().length() == 0  && pointeuse.getComboSelected() != null) {
				
				int i = 0;
				
				/*On met le nom contenue dans le JComboBox dans name et le prenom dans firstName*/
				String[] split = pointeuse.getComboSelected().split(" ", 0);
				String name = new String(split[1]);
				String firstName = new String(split[0]);
				
				/*empTemp prend la valeur de chaque objet employee contenue dans le fichier employee*/
				for(Employee x : Serialisation.DeserializeEmployee()) {
					
					/*Si firstName et name sont égales aux noms et prénom d'un employé du fichier */
					if(firstName.equals(x.getFirstName()) && name.equals(x.getName())) {
						JOptionPane.showMessageDialog(pointeuse, "Validated Score", "Validation",javax.swing.JOptionPane.ERROR_MESSAGE);
						scoring.setEmployee(x);//on ajoute l'employé concerné dans scoring
					}
				}
			}
			
			/*Si aucun champ n'a été remplie dans la pointeuse*/
			if(pointeuse.gettxtID().getText().length() == 0 && pointeuse.getComboSelected() == null) {
				JOptionPane.showMessageDialog(pointeuse, "You have to write in ID field or Name field", "Erreur",javax.swing.JOptionPane.ERROR_MESSAGE);
			}
			
			/*Si les deux champs ont été remplie dans la pointeuse*/
			if(pointeuse.gettxtID().getText().length() != 0 && pointeuse.getComboSelected() != null) {
				JOptionPane.showMessageDialog(pointeuse, "You have to write in only one field", "Erreur",javax.swing.JOptionPane.ERROR_MESSAGE);
			}
			
			Serialisation.SerializeScoring(scoring);//On sérialise scoring qui contient les informations du pointage.
		}
		
		/*Si on appuie sur checkOut*/
		if(e.getActionCommand() == "checkOut") {
			
			/*Le nouvel objet Scoring prend la date et l'heure actuelle*/
			Scoring scoring = new Scoring();
			scoring.setDateScore(pointeuse.getDate());
			scoring.setDepTime(pointeuse.getAroundTime());
			
			/*Si le champ contenant l'ID n'est pas vide et le champ contenant les nom et prenom est vide*/
			if(pointeuse.gettxtID().getText().length() != 0 && pointeuse.getComboSelected() == null) {
				try {
					int ID, cmpt = 0;
					
					ID = Integer.parseInt(pointeuse.gettxtID().getText());//ID contient la valeur rentré dans le champ de l'ID
					
					/*empTemp prend la valeur de chaque objet employee contenue dans le fichier employee*/
					for(Employee empTemp : Serialisation.DeserializeEmployee()) {
						
						/*Si l'ID d'un employé du fichier correspond à l'ID rentré dans la pointeuse*/
						if(empTemp.getIdEmp() == ID) {
							JOptionPane.showMessageDialog(pointeuse, "Validated Score", "Validation",javax.swing.JOptionPane.ERROR_MESSAGE);
							scoring.setEmployee(empTemp);//On ajoute l'employé correspondant dans le nouvel objet scoring.
						}
						
						/*Pour chaque employee du fichier n'ayant pas l'ID rentré dans la pointeuse*/
						if(empTemp.getIdEmp() != ID) {
							cmpt++; //on incrémente cmpt;
						}
					}
					
					/*Si aucun ID rentré dans la pointeuse ne correspond au employee du fichier*/
					if(cmpt == Serialisation.DeserializeEmployee().size()) {
						JOptionPane.showMessageDialog(pointeuse, "Acknowledged ID", "Erreur",javax.swing.JOptionPane.ERROR_MESSAGE);
					}
				}
				
				/*Exception sur le format du nombre rentré dans le champ ID de la pointeuse*/
				catch(NumberFormatException nbr) {
					JOptionPane.showMessageDialog(pointeuse, "Format Error ID", "Erreur",javax.swing.JOptionPane.ERROR_MESSAGE);
					nbr.printStackTrace();
				}
			}
			
			/*Si le champ contenant l'ID est vide et l'utilisateur a sélectionné un nom et prenom*/
			if(pointeuse.gettxtID().getText().length() == 0  && pointeuse.getComboSelected() != null) {
				
				int i = 0;
				
				/*On met le nom contenue dans le JComboBox dans name et le prenom dans firstName*/
				String[] split = pointeuse.getComboSelected().split(" ", 0);
				String name = new String(split[1]);
				String firstName = new String(split[0]);
				
				/*empTemp prend la valeur de chaque objet employee contenue dans le fichier employee*/
				for(Employee x : Serialisation.DeserializeEmployee()) {
					
					/*Si firstName et name sont égales aux noms et prénom d'un employé du fichier */
					if(firstName.equals(x.getFirstName()) && name.equals(x.getName())) {
						JOptionPane.showMessageDialog(pointeuse, "Validated Score", "Validation",javax.swing.JOptionPane.ERROR_MESSAGE);
						scoring.setEmployee(x);//on ajoute l'employé concerné dans scoring
					}
				}
			}
			
			/*Si aucun champ n'a été remplie dans la pointeuse*/
			if(pointeuse.gettxtID().getText().length() == 0 && pointeuse.getComboSelected() == null) {
				JOptionPane.showMessageDialog(pointeuse, "You have to write in ID field or Name field", "Erreur",javax.swing.JOptionPane.ERROR_MESSAGE);
			}
			
			/*Si les deux champs ont été remplie dans la pointeuse*/
			if(pointeuse.gettxtID().getText().length() != 0 && pointeuse.getComboSelected() != null) {
				JOptionPane.showMessageDialog(pointeuse, "You have to write in only one field", "Erreur",javax.swing.JOptionPane.ERROR_MESSAGE);
			}
			
			Serialisation.SerializeScoring(scoring);//On sérialise scoring qui contient les informations du pointage.
		}	
	}
}