import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionTime implements ActionListener{
	
	private IHMPointeuse pointeuse;
	
	public ActionTime(IHMPointeuse pointeuse) {
		super();
		this.pointeuse = pointeuse;
	}
	
	public void actionPerformed(ActionEvent e) {
		pointeuse.setLabTime();
	}
}