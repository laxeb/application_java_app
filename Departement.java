package application_java_app;

import java.util.ArrayList;

public class Departement implements Serializable{
	private String nameDep;
	private final long serialVersionUID = 2L;
	private ArrayList<Employee> employee ;

	/**
	 * @brief default builder
	 */
	public Departement() {
		employee = new ArrayList<Employee>();
	}

	/**
	 * @brief builder with the name of the department
	 * @param nameDep the name
	 */
	public Departement(String nameDep) {
		this.nameDep = nameDep;
		employee = new ArrayList<Employee>();
	}

	/**
	 * @brief builder with the name of the department and the list of employees
	 * @param nameDep the name of the department
	 * @param employee the list of employees
	 */
	public Departement(String nameDep, ArrayList<Employee> employee) {
		this.nameDep = nameDep;
		this.employee = new ArrayList<Employee>(employee);
	}
	
	/**
	 * @brief nameDep getter
	 */
	public String getNameDep() {
		return nameDep;
	}
	
	/**
	 * @brief employee list getter
	 */
	public ArrayList<Employee> getListEmp() {
		return employee;
	}
	
	
	/**
	 * @brief nameDep setter
	 */
	public void setNameDep(String nameDep) {
		this.nameDep = nameDep;
	}

	/**
	 * @brief employee list setter
	 */
	public void setListEmp(ArrayList<Employee> employee) {
		this.employee = new ArrayList<Employee>(employee);
	}
	
	
	/**
	 * @brief adds an employee to the list of employees
	 * @param EmpParam the employee to add
	 */
	public void addEmployee(Employee EmpParam) {
		if(employee.contains(EmpParam)){
			throw new IllegalArgumentException("Employ� d�j� r�ferenc� !");
		}
		else {
			employee.add(EmpParam);
		}
	}
	
	/**
	 * @brief remove an employee of the list of employees
	 * @param EmpParam the employee to remove
	 */
	public void removeEmployee(Employee EmpParam) {
		if(!employee.contains(EmpParam)){
			throw new IllegalArgumentException("Employe non reference !");
		}
		else {
			int i = 0;
			//find the employee to remove
			for (Employee EmpTemp : employee )
				if (EmpTemp == EmpParam) {
					employee.remove(i);		
				}
			++i;
			}
	}
	
	/**
	 * @brief displays the list of employees
	 */
	public void displayEmployees() {
		for (Employee Emp : employee) {
			Emp.displayEmp();
		}
	}

	/**
	 * @brief change a department object to string
	 */
	public String toString() {
		String str = "Name of the departement : " + getNameDep() + "\n" +
				"List of the employees : " + getListEmp();
		return str;
	}
}
