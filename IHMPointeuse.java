import java.awt.Container;
import java.awt.Font;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import javax.swing.*;


class IHMPointeuse extends JFrame 
{
	/*Declaration des JLabel*/
	private JLabel labTitle = new JLabel ("Pointeuse");  
	private JLabel labDate = new JLabel(LocalDate.now().toString());  
	private JLabel labTime = new JLabel();
	private JLabel labAroundTime = new JLabel();
	
	/*Déclaration des timers*/
	private Timer timer2 = new Timer(500, new ActionAroundTime(this));
	private Timer timer = new Timer(500, new ActionTime(this));
	
	/*Déclaration du JTextField , des bouttons, du conteneur et du JComboBox*/
	private JTextField txtID = new JTextField (null);
	private JButton checkIn = new JButton("CheckIn");
	private JButton checkOut = new JButton("CheckOut");
	private Container contenu = new Container();
    private JComboBox boxNom = new JComboBox(new ActionPointeuse(this).getNameEmp());

    public IHMPointeuse()
    {
    		 super(); 
    		 
    		 /*JLabel servant pour les écritures immuable de la pointeuse*/
    		 JLabel preDate = new JLabel("Current Time :");
    		 JLabel OR = new JLabel("OR");
    		 JLabel preAroundDate = new JLabel("Rounded time :");
    		 JLabel preID = new JLabel("ID Employee :");
    		 JLabel preName = new JLabel("Choose a name :");
    		 
    		 /*Choix de la police d'écriture et de la couleur*/
    		 labTitle.setFont(new java.awt.Font("Brixton",Font.BOLD,30));
    		 labDate.setFont(new java.awt.Font("Brixton",Font.BOLD,18));
    		 labTime.setFont(new java.awt.Font("Brixton",Font.BOLD,15));
    		 labAroundTime.setFont(new java.awt.Font("Brixton",Font.BOLD,15));
    		 preDate.setFont(new java.awt.Font("Brixton",Font.BOLD,14));
    		 OR.setFont(new java.awt.Font("Brixton",Font.BOLD,15));
    		 preID.setFont(new java.awt.Font("Brixton",Font.BOLD,15));
    		 preName.setFont(new java.awt.Font("Brixton",Font.BOLD,15)); 
    		 preAroundDate.setFont(new java.awt.Font("Brixton",Font.BOLD,14));

    		 ActionPointeuse actionCheck = new ActionPointeuse(this);
             
    		 /*Timer pour avoir l'heure exact en temps réel*/
        	 timer.setRepeats(true);
        	 timer.setCoalesce(true);
        	 timer.setInitialDelay(0);
        	 timer.start();
        	 
        	 /*Timer pour avoir l'heure arrondie au quart d'heure en temps réel*/
        	 timer2.setRepeats(true);
        	 timer2.setCoalesce(true);
        	 timer2.setInitialDelay(0);
        	 timer2.start();
    
             /*On associe un actionListener aux deux bouttons*/
        	 checkIn.addActionListener(actionCheck);
             checkIn.setActionCommand("checkIn");
             checkOut.addActionListener(actionCheck);
             checkOut.setActionCommand("checkOut");
             
             /*On ajoute tous les attributs au contenue de la fenêtre*/
             contenu = getContentPane();
             contenu.add(preDate);
             contenu.add(preID);
             contenu.add(OR);
             contenu.add(preAroundDate);
             contenu.add(checkIn);
             contenu.add(checkOut);
             contenu.add(preName);
             contenu.add(boxNom);
             contenu.add (labTitle) ;
             contenu.add (txtID ) ;
             contenu.add(labDate);
             contenu.add(labAroundTime);
             contenu.add(labTime);
             contenu.setLayout(null);
             
             /*Disposition des éléments sur la page*/
             labTitle.setBounds(215,20,150,40) ;
             txtID.setBounds(190,260,200,30) ; 
             preDate.setBounds(60,160,120,20);
             preAroundDate.setBounds(330,160,120,20);
             boxNom.setBounds(190,380,200,30);
             preID.setBounds(60,260,150,30);
             OR.setBounds(275,305,50,50);
             preName.setBounds(60,380,200,30);
             checkIn.setBounds(10,500,200,40);
             checkOut.setBounds(375,500,200,40);
             labDate.setBounds(230, 100, 150, 20);
             labAroundTime.setBounds(440, 160, 100, 20);
             labTime.setBounds(170, 160, 100, 20);
             
             /*options de la fenêtre*/
             this.setAlwaysOnTop(true);
             this.setBounds(20,20,600,600);
             this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
             this.setVisible(true);
    }
     
    /*Change la valeur du JTextFiled*/
     public void setID(JTextField txtIDSet) {
    	this.txtID = txtIDSet;
     }
    
     /*recupere le JTextField*/
     public JTextField gettxtID() {
    	 return txtID;
     }
     
     /*recupere le champs selectionné dans la JComboBox*/
     public String getComboSelected() {
    	 return (String)boxNom.getSelectedItem();
     }
     
     /*recupere la date*/
     public LocalDate getDate() {
    	 return LocalDate.now();
     }
     
     /*recupere l'heure exact*/
     public LocalTime getTime() {
    	 return LocalTime.now(); 
     }
     
     /*Recupère l'heure arrondie*/
     public LocalTime getAroundTime() {
    	 DateTimeFormatter f1=DateTimeFormatter.ofPattern("HH : mm");
    	 LocalTime aroundTime = LocalTime.parse(labAroundTime.getText(), f1);
    	 return aroundTime;
     }
     
     /*Sert à changer l'heure arrondie au quart d'heure en temps réel*/
     public void setAroundTime() {
    	 LocalTime time = LocalTime.now(); 
    	 LocalTime lastQuarter = time.truncatedTo(ChronoUnit.HOURS) .plusMinutes(15 * (time.getMinute() / 15)); 
    	 DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH : mm");
    	 String strTime = lastQuarter.format(formatter);
    	 labAroundTime.setText(strTime);
     }
     
    /*Sert à changer l'heure en temps réel*/
    public void setLabTime() {
    	labTime.setText(DateFormat.getTimeInstance().format(new Date()));
    }
    
  }