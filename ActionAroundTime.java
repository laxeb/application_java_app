import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionAroundTime implements ActionListener{
	
	private IHMPointeuse pointeuse;
	
	public ActionAroundTime(IHMPointeuse pointeuse) {
		super();
		this.pointeuse = pointeuse;
	}
	
	public void actionPerformed(ActionEvent e) {
		pointeuse.setAroundTime();
	}
}