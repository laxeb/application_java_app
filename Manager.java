package application_java_app;

public class Manager extends Employee {
	/**
	 * @brief adds an employee to the managed department
	 * @param EmpParam the employee to add
	 */
	 public void addEmployee(Employee EmpParam) {
		 try{
			 getDepartement().addEmployee(EmpParam);
			 EmpParam.setDepartement(getDepartement());
		 }
		 catch (IllegalArgumentException EXC){
			 throw EXC;
		 }
	}
	
	 
	 
	/**
	 * @brief remove an employee of the managed department
	 * @param EmpParam the employee to remove
	 */
	public void removeEmployee(Employee EmpParam) {
		try{
			 getDepartement().removeEmployee(EmpParam);

		 }
		 catch (IllegalArgumentException EXC){
			 throw EXC;
		 }
	}
	
	/**
	 * @brief displays the list of employees of the managed department
	 */
	public void displayEmployees() {
		getDepartement().displayEmployees();
	}
}
