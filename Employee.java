package application_java_app;

import java.util.ArrayList;

public class Employee  implements Serializable{

	private final long serialVersionUID = 4L;

	private static int idMaxEmp;
	
	private int idEmp;

	private String nameEmp;

	private String firstNameEmp;

	private Departement departement ;

	private ArrayList<PlanningHebdo> planningHebdo = new ArrayList<PlanningHebdo>();
	
	private ArrayList<Scoring> scoring = new ArrayList<Scoring>();

	/**
	 * @brief default builder
	 */
	public Employee() {
		++idMaxEmp;
		idEmp = idMaxEmp;
		nameEmp = null;
		firstNameEmp = null;
		departement = null;
		
	}

	/**
	 * @brief builder with names
	 */
	public Employee(String newName, String newFirstName) {
		++idMaxEmp;
		idEmp = idMaxEmp;
		nameEmp = newName;
		firstNameEmp = newFirstName;
		departement = null;
	}
	
	/**
	 * @brief builder with all attributes except for past scoring
	 */
	public Employee(String newName, String newFirstName, Departement dep, ArrayList<PlanningHebdo> newPlanningHebdo) {
		++idMaxEmp;
		idEmp = idMaxEmp;
		nameEmp = newName;
		firstNameEmp = newFirstName;
		departement = dep;
		planningHebdo = newPlanningHebdo;
	}
	
	/**
	 * @brief id getter
	 */
	public int getIdEmp() {
		return idEmp;
	}
	
	/**
	 * @brief name getter
	 */
	public String getName() {
		return nameEmp;
	}

	/**
	 * @brief first name getter
	 */
	public String getFirstName() {
		return firstNameEmp;
	}
	
	/**
	 * @brief departement getter
	 */
	public Departement getDepartement() {
		return departement;
	}

	/**
	 * @brief planning list getter
	 */
	public ArrayList<PlanningHebdo> getListPlanning() {
		return planningHebdo;
	}
	
	/**
	 * @brief scoring list getter
	 */
	public ArrayList<Scoring> getListScoring() {
		return scoring;
	}

	/**
	 * @brief name setter
	 * @param newName the new name
	 */
	public void setName(String newName) {
		nameEmp = newName;
	}

	/**
	 * @brief first name setter
	 * @param newFirstName the new first name
	 */
	public void setFirstName(String newFirstName) {
		firstNameEmp = newFirstName;

	}
	
	/**
	 * @brief departement setter
	 * @param newDepartement the new departement
	 */
	public void setDepartement(Departement newDepartement) {
		departement = newDepartement;

	}

	/**
	 * @brief planning list setter
	 * @param newPlanningHebdo the new planning
	 */
	public void setListPlan(ArrayList<PlanningHebdo> newPlanningHebdo) {
		planningHebdo = newPlanningHebdo;

	}
	
	/**
	 * @brief scoring list setter
	 * @param newScoring the new scoring
	 */
	public void setListScoring(ArrayList<Scoring> newScoring) {
		scoring = newScoring;

	}
	
	/**
	 * @brief add a planning
	 * @param planningParam the planning to add
	 */
	public void addPlanning(PlanningHebdo planningParam) {
		planningHebdo.add(planningParam);
	}
	
	
	/**
	 * @brief add a scoring
	 * @param scoringParam the scoring to add
	 */
	public void addScoring(Scoring scoringParam) {
		scoring.add(scoringParam);
	}
	
	
	/**
	 * @brief displays an employee
	 */
	public void displayEmp() {
		System.out.println("Identifiant employe : " + idEmp);
		System.out.println("Nom : " + nameEmp);
		System.out.println("Prenom : " + firstNameEmp);
		System.out.println("Departement : " + departement.getNameDep());
		//display planning ??
	}


	/*public localTime extraHourWeek() {
		return null;
	}
	*/


}

